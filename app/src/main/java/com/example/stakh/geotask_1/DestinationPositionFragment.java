package com.example.stakh.geotask_1;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DestinationPositionFragment extends Fragment {
    private DestinationAddressCallback callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );

        if (context instanceof DestinationAddressCallback)
            callback = (DestinationAddressCallback) context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate( R.layout.destination_position_fragment, container, false );

        final AutoCompleteTextView destinationCity = v.findViewById( R.id.tv_destination_city );
        destinationCity.setDropDownHeight(getResources().getDimensionPixelSize(R.dimen.dropdown_height));

        final PlacesAutoCompleteAdapter placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter( getActivity(), R.layout.ac_tv_destination_city);
        destinationCity.setAdapter( placesAutoCompleteAdapter );
        destinationCity.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = placesAutoCompleteAdapter.getItem( position );

                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocationName(name, 1);
                    if (!addresses.isEmpty()) {

                        Address address = addresses.get( 0 );
                        callback.onDestinationAddressSelected( address, name );
                    }
                } catch (IOException e) {
                    Log.e( "Can't find address", e.getMessage() );
                }
            }
        } );
        destinationCity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                destinationCity.setText(null);
                Toast.makeText(getActivity(), "End Address Is Clear", Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        destinationCity.setFocusableInTouchMode( true );
        destinationCity.requestFocus();

        return v;
    }
    interface DestinationAddressCallback {
        void onDestinationAddressSelected(Address address, String name);
    }
}
