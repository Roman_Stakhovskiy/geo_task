package com.example.stakh.geotask_1;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String PACKAGE_NAME = "com.example.stakh.geotask_1";
    public static final String EXTRA_LAST_LOCATION_LATITUDE = PACKAGE_NAME + "_last_user_location_lat";
    public static final String EXTRA_LAST_LOCATION_LONGITUDE = PACKAGE_NAME + "_last_user_location_lng";

    public static final String EXTRA_START_ADDRESS_LOCATION_LATITUDE = PACKAGE_NAME + "_start_address_location_lat";
    public static final String EXTRA_START_ADDRESS_LOCATION_LONGITUDE = PACKAGE_NAME + "_start_address_location_lng";
    public static final String EXTRA_START_ADDRESS_LOCATION = PACKAGE_NAME + "_start_address_location";

    public static final String EXTRA_DESTINATION_ADDRESS_LOCATION_LATITUDE = PACKAGE_NAME + "_DESTINATION_address_location_lat";
    public static final String EXTRA_DESTINATION_ADDRESS_LOCATION_LONGITUDE = PACKAGE_NAME + "_DESTINATION_address_location_lng";
    public static final String EXTRA_DESTINATION_ADDRESS_LOCATION = PACKAGE_NAME + "_destinationAddress_address_location";

    private GoogleMap googleMap;

    private LatLng startAddressMarker;
    private LatLng destinationAddressMarker;
    private LatLng lastUserLocation;

    private String startAddress, destinationAddress;


    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);
        getDataFromIntent();

        // Getting reference to SupportMapFragment of the activity_main
        SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_get_way);
        fm.getMapAsync(this);

        String url = getDirectionsUrl(startAddressMarker, destinationAddressMarker);
        DownloadTask downloadTask = new DownloadTask();
        // Start downloading json data from Google Directions API
        downloadTask.execute(url);

//        try {
//            Toast.makeText(this, "Found!", Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Toast.makeText(this, "Unknown!", Toast.LENGTH_LONG).show();
//        }
    }

    public void getDataFromIntent() {
        double defaultValue = -1;

        Intent directionsIntent = getIntent();
        double lastUserLocationLat = directionsIntent.getDoubleExtra(EXTRA_LAST_LOCATION_LATITUDE, defaultValue);
        double lastUserLocationLng = directionsIntent.getDoubleExtra(EXTRA_LAST_LOCATION_LONGITUDE, defaultValue);

        double startAddressLat = directionsIntent.getDoubleExtra(EXTRA_START_ADDRESS_LOCATION_LATITUDE, defaultValue);
        double startAddressLng = directionsIntent.getDoubleExtra(EXTRA_START_ADDRESS_LOCATION_LONGITUDE, defaultValue);
        startAddress = directionsIntent.getStringExtra(EXTRA_START_ADDRESS_LOCATION);

        double destinationAddressLat = directionsIntent.getDoubleExtra(EXTRA_DESTINATION_ADDRESS_LOCATION_LATITUDE, defaultValue);
        double destinationAddressLng = directionsIntent.getDoubleExtra(EXTRA_DESTINATION_ADDRESS_LOCATION_LONGITUDE, defaultValue);
        destinationAddress = directionsIntent.getStringExtra(EXTRA_DESTINATION_ADDRESS_LOCATION);

        if (lastUserLocationLat != defaultValue && lastUserLocationLng != defaultValue) {
            lastUserLocation = new LatLng(lastUserLocationLat, lastUserLocationLng);
        }
        if (startAddressLat != defaultValue && startAddressLng != defaultValue) {
            startAddressMarker = new LatLng(startAddressLat, startAddressLng);
        }
        if (destinationAddressLat != defaultValue && destinationAddressLng != defaultValue) {
            destinationAddressMarker = new LatLng(destinationAddressLat, destinationAddressLng);
        }
    }

    public void onMapReady(GoogleMap map) {
        googleMap = map;
        int padding = 100;

        if (map != null) {
            UiSettings uiSettings = map.getUiSettings();

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            map.setMyLocationEnabled(true);
            uiSettings.setZoomControlsEnabled(true);
            uiSettings.setMyLocationButtonEnabled(false);

            LatLngBounds.Builder latLngsBounds = new LatLngBounds.Builder()
                    .include(startAddressMarker)
                    .include(destinationAddressMarker)
                    .include(lastUserLocation);

            addMarker(startAddress, startAddressMarker, R.drawable.marker_blue_a, map);
            addMarker(destinationAddress, destinationAddressMarker, R.drawable.marker_green_b, map);

            map.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngsBounds.build(), padding));
        }
    }

    private void addMarker(String title, LatLng position, int resource, GoogleMap map) {

        MarkerOptions markerLocation = new MarkerOptions();
        markerLocation.position(position).title(title)
                .icon(BitmapDescriptorFactory.fromResource(resource));
        map.addMarker(markerLocation);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&key=" + PlaceAPI.API_KEY;
        // Output format
        String output = "json";
        // Building the url to the web service

        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        HttpURLConnection urlConnection;
        URL url = new URL(strUrl);

        // Creating an http connection to communicate with url
        urlConnection = (HttpURLConnection) url.openConnection();
        // Connecting to url
        urlConnection.connect();
        // Reading data from url
        try (InputStream iStream = urlConnection.getInputStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception downloading", e.toString());
        } finally {
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser directionsJSONParser = new DirectionsJSONParser();
                // Starts parsing data
                routes = directionsJSONParser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);
                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                // Adding all the points in the route to LineOptions
                lineOptions = new PolylineOptions()
                        .addAll(points)
                        .geodesic(true)
                        .width(10)
                        .color(Color.BLACK);
            }
            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                googleMap.addPolyline(lineOptions);
                Toast.makeText(getBaseContext(), "Found!", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(getBaseContext(), "Sorry, Direction Not Found, Please Enter New Address!", Toast.LENGTH_LONG).show();
            }
        }
    }
}

