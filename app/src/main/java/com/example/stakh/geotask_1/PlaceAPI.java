package com.example.stakh.geotask_1;
import android.util.Log;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PlaceAPI {

    private static final String TAG = PlaceAPI.class.getSimpleName();
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    public static final String API_KEY = "AIzaSyBLu8-CRCnVNuibUHWsEjfIJAY1Qm7-u4c";

    ArrayList<String> autocomplete(String input) {

        ArrayList<String> resultList;
        resultList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();

        try {
            String sb = new StringBuilder()
                    .append( PLACES_API_BASE )
                    .append( TYPE_AUTOCOMPLETE )
                    .append( OUT_JSON )
                    .append( "?input=" )
                    .append( URLEncoder.encode( input, "utf8" ) )
                    .append( "&types=(cities)" )
                    .append( "&key=" )
                    .append( API_KEY ).toString();

            URL url = new URL( sb );
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader( conn.getInputStream() );

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read( buff )) != -1) {
                jsonResults.append( buff, 0, read );
            }
        } catch (MalformedURLException e) {
            Log.e( TAG, "Error processing Places API URL", e );
            return resultList;
        } catch (IOException e) {
            Log.e( TAG, "Error connecting to Places API", e );
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            Log.d( TAG, jsonResults.toString() );

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject( jsonResults.toString() );
            JSONArray predsJsonArray = jsonObj.getJSONArray( "predictions" );

            // Extract the Place descriptions from the results
            resultList = new ArrayList<>( predsJsonArray.length() );
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add( predsJsonArray.getJSONObject( i ).getString( "description" ) );
            }

        } catch (JSONException e) {
            Log.e( TAG, "Cannot process JSON results", e );
        }
        return resultList;
    }
}



