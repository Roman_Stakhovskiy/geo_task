package com.example.stakh.geotask_1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;

public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<String> mResultList;
    private PlaceAPI mPlaceAPI = new PlaceAPI();



    public PlacesAutoCompleteAdapter(Context context, int resource) {
        super( context, resource );
    }

    @Override
    public int getCount() {
        return mResultList.size();
    }

    @Override
    public String getItem(int position) {
        return mResultList.get( position );

    }

    @NonNull
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null && constraint.toString().length() >= 3) {
                    mResultList = mPlaceAPI.autocomplete( constraint.toString() );

                    filterResults.values = mResultList;
                    filterResults.count = mResultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

}