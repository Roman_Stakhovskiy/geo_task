package com.example.stakh.geotask_1;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class StartPositionFragment extends Fragment {

    private StartAddressCallback callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );

        if (context instanceof StartAddressCallback)
            callback = (StartAddressCallback) context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.start_position_fragment, container, false);

        final AutoCompleteTextView startCity = v.findViewById(R.id.tv_start_city);
        startCity.setDropDownHeight(getResources().getDimensionPixelSize(R.dimen.dropdown_height));

        final PlacesAutoCompleteAdapter placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.ac_tv_start_city);
        startCity.setAdapter(placesAutoCompleteAdapter);
        startCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                final String name = placesAutoCompleteAdapter.getItem(position);
                final Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocationName(name, 1);

                    if (!addresses.isEmpty()) {
                        Address address = addresses.get(0);
                        callback.onStartAddressSelected(address, name);

                    }
                } catch (IOException e) {
                    Log.e("Can't find address", e.getMessage());
                }
            }
        });

        startCity.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                startCity.setText(null);
                Toast.makeText(getActivity(), "Start Address Is Clear", Toast.LENGTH_SHORT).show();

                return false;
            }
        });
        startCity.setFocusableInTouchMode(true);
        startCity.requestFocus();

        return v;
    }

    interface StartAddressCallback {
        void onStartAddressSelected(Address address, String name);
    }
}
