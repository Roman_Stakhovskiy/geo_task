package com.example.stakh.geotask_1;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class TabsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, StartPositionFragment.StartAddressCallback,
        DestinationPositionFragment.DestinationAddressCallback, View.OnClickListener {

    private static final String TAG = "googlemap_market";
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;

    private Marker startAddresMarker, destinationAddressMarker;
    private Boolean  bs_netcheck = false;

    private boolean locationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 100;
    private LatLng lastKnownUserLocation;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location lastLocation = locationResult.getLastLocation();

            if (lastLocation != null)
                lastKnownUserLocation = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bs_netcheck = netCheck();
        if(bs_netcheck)
        {
            displayAlert();
        }

        setContentView(R.layout.tabs_activity);

        TabsFragment fragmentTab = new TabsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.item_detail_container, fragmentTab).commit();

        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this)
                .addConnectionCallbacks(this).addApi(LocationServices.API).addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API).addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLocationPermission();

        Button getWayBtn = findViewById(R.id.direction);
        getWayBtn.setOnClickListener(this);
    }

    private boolean checkLocationPermission() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        googleMap = map;

        updateLocationUI();
        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);

        if (checkLocationPermission())
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }
    public boolean netCheck(){

        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        if ( connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                && connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED ) {
            bs_netcheck = true;
        }else {
            bs_netcheck =  false;
        }
        return bs_netcheck;
    }

    public void displayAlert(){

        final Context ctx;
        ctx = TabsActivity.this;
        new AlertDialog.Builder(this).setMessage("Please, Check Your Internet Connection and Try Again")
                .setTitle("Network Error")
                .setCancelable(true)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton){
                                finish();
                            }
                        })
        .setPositiveButton(R.string.settings_button_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which){
                ctx.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            }
        })
                .show();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fusedLocationClient != null && checkLocationPermission())
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (fusedLocationClient != null)
            fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended");
        if (cause == CAUSE_NETWORK_LOST)
            Log.e(TAG, "onConnectionSuspended(): Google Play services " + "connection lost.  Cause: network lost.");
        else if (cause == CAUSE_SERVICE_DISCONNECTED)
            Log.e(TAG, "onConnectionSuspended():  Google Play services " + "connection lost.  Cause: service disconnected");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void updateLocationUI() {
        if (googleMap == null) {
            return;
        }
        try {
            if (locationPermissionGranted) {
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                googleMap.setMyLocationEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        locationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    @Override
    public void onStartAddressSelected(Address address, String name) {
        if (startAddresMarker != null) startAddresMarker.remove();
        startAddresMarker = addMarkerOnMap(address, name, R.drawable.marker_blue_a);
    }

    @Override
    public void onDestinationAddressSelected(Address address, String name) {
        if (destinationAddressMarker != null) destinationAddressMarker.remove();
        destinationAddressMarker = addMarkerOnMap(address, name, R.drawable.marker_green_b);
    }

    private Marker addMarkerOnMap(Address address, String name, int resource) {
        if (googleMap != null) {
            MarkerOptions markerLocation = new MarkerOptions();
            LatLng addressPosition = new LatLng(address.getLatitude(), address.getLongitude());
            markerLocation.position(addressPosition).title(name).icon(BitmapDescriptorFactory.fromResource(resource));
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(addressPosition));
            return googleMap.addMarker(markerLocation);
        }
        return null;
    }

    @Override
    public void onClick(View v) {

        Intent directionsIntent = new Intent(this, DirectionsActivity.class);

        if (startAddresMarker!= null && destinationAddressMarker != null && lastKnownUserLocation != null) {
            directionsIntent.putExtra(DirectionsActivity.EXTRA_LAST_LOCATION_LATITUDE, lastKnownUserLocation.latitude);
            directionsIntent.putExtra(DirectionsActivity.EXTRA_LAST_LOCATION_LONGITUDE, lastKnownUserLocation.longitude);

            directionsIntent.putExtra(DirectionsActivity.EXTRA_START_ADDRESS_LOCATION_LATITUDE, startAddresMarker.getPosition().latitude);
            directionsIntent.putExtra(DirectionsActivity.EXTRA_START_ADDRESS_LOCATION_LONGITUDE, startAddresMarker.getPosition().longitude);
            directionsIntent.putExtra(DirectionsActivity.EXTRA_START_ADDRESS_LOCATION, startAddresMarker.getTitle());

            directionsIntent.putExtra(DirectionsActivity.EXTRA_DESTINATION_ADDRESS_LOCATION_LATITUDE, destinationAddressMarker.getPosition().latitude);
            directionsIntent.putExtra(DirectionsActivity.EXTRA_DESTINATION_ADDRESS_LOCATION_LONGITUDE, destinationAddressMarker.getPosition().longitude);
            directionsIntent.putExtra(DirectionsActivity.EXTRA_DESTINATION_ADDRESS_LOCATION, destinationAddressMarker.getTitle());
        }

        if (startAddresMarker == null) {
            Toast.makeText(this, "Please, Enter The Start Address", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destinationAddressMarker == null) {
            Toast.makeText(this, "Please, Enter The End Address", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(directionsIntent);

        overridePendingTransition(R.anim.top_out, R.anim.bottom_in);
    }
}