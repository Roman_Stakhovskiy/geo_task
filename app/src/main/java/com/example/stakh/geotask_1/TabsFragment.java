package com.example.stakh.geotask_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TabsFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tabs, container, false);

        FragmentTabHost mTabHost = (FragmentTabHost) rootView.findViewById(android.R.id.tabhost);
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.tabs_activity);

        mTabHost.addTab(mTabHost.newTabSpec("Start").setIndicator("Start"),
                StartPositionFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("End").setIndicator("End"),
                DestinationPositionFragment.class, null);

        return rootView;
    }

}